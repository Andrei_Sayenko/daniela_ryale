/**
 * Created by Andrey on 20.07.2016.
 */

$(document).ready(function(){
    function PopUpShow(){
        $("#popup1").show();
    }

    function PopUpHide(){
        $("#popup1").hide();
    }

    $(".back_call").click(function(){
        PopUpShow();
        return false;
    });

    $(".b_popup").click(function(event){
        var target = event.target,
            targetClass = target.classList;

        if(targetClass.contains('hide_popup') || targetClass.contains('b_popup') || targetClass.contains('exit_popup')) {
            PopUpHide();
            return false;
        } else if (targetClass.contains('submit_number')) {
            PopUpHide();
        }
    });

    $('.submit_number').prop( "disabled", true );

    $('.input_number')[0].addEventListener('input', function () {
        var numberValue =  this.value.length;

        if ( numberValue!==13) {
            $('.submit_number').prop( "disabled", true );
        } else {
            $('.submit_number').prop( "disabled", false );
        }
    });
});

$(document).ready(function() {
    function ChatShow() {
        $("#online_chat").show();
    }

    function ChatHide() {
        $("#online_chat").hide();
    }

    $(".chat").click(function () {
        ChatShow();
        return false;
    });

    $(".b_popup").click(function (event) {
        var target = event.target,
            targetClass = target.classList;

        if (targetClass.contains('hide_chat') || targetClass.contains('b_popup') || targetClass.contains('exit_chat')) {
            ChatHide();
            return false;
        }
    });

    function sendMessage() {
        var textMessage = $("#sendie")[0];

        $(".chat_window")[0].innerHTML +=  '<div class="user_message clear">' +
            '<div class="message">' +
            '<p>' + textMessage.value + '</p>' +
            '</div>' +
            '</div>';
        textMessage.value = '';
    }

    $(".send_button").click(sendMessage);
    $("#sendie")[0].onkeypress = function (event) {
        var eKeyCode = event.keyCode;

        if (eKeyCode == '13') {
            sendMessage();
        }
    }
});





